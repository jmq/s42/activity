//querySelector is used to select a specific element

// document refers to the whole page
console.log(document.querySelector('#txt-first-name'))


//getElementById directly targe the ID name 
/*
*	document.getElementById('txt-first-name')
*	document.getElementByClassName()
*	document.getElementByTagName()
*/



const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
let spanFullName = document.querySelector('#span-full-name')

//Event Listeners
/*
	selectedElement.addEventListener('event', function)
*/

// txtFirstName.addEventListener('keyup', event => {
// 	spanFullName.innerHTML = txtFirstName.value
// })

txtFirstName.addEventListener('keyup', printFullName)
txtLastName.addEventListener('keyup', printFullName)

function printFullName() {
	spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value
}